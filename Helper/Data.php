<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 23/12/16
 * Time: 12:26
 */

namespace Igorludgero\Outofstocklast\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $_stockItemRepository;
    protected $_scopeConfig;
    protected $_storeScope;

    public function __construct(\Magento\CatalogInventory\Model\Stock\StockItemRepository $_stockItemRepository, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->_stockItemRepository = $_stockItemRepository;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
    }

    /**
     * @param $ids -> Product Ids to check stock.
     * @return array -> Return the product is in the correct order.
     */
    public function getIdsToLastStock($ids){
        if($this->_scopeConfig->getValue('outofstocklast/igorludgero_outofstocklast/active',$this->_storeScope)==1) {
            $products = array();
            $productIds = array();
            foreach ($ids as $id) {
                try {
                    $stockItem = $this->_stockItemRepository->get($id);
                    $products[] = array('id' => $id, 'stock' => $this->checkIfAvailableInStock($stockItem));
                } catch (\Exception $ex) {
                    $products[] = array('id' => $id, 'stock' => 0);
                }
            }

            if (count($products) > 0) {
                foreach ($products as $id => $value) {
                    $names[$id] = $value['stock'];
                }
                $keys = array_keys($products);
                array_multisort(
                    $names, SORT_ASC, SORT_NUMERIC, $products, $keys
                );
                $result = array_combine($keys, $products);
                foreach ($result as $product) {
                    $productIds[] = intval($product['id']);
                }
                return $productIds;
            } else {
                return $ids;
            }
        }
        return $ids;
    }

    /**
     * @param $item -> The product item repository.
     * @return int -> The state if is in stock or not.
     */
    private function checkIfAvailableInStock($item){
        if($item->getIsInStock()){
            if($item->getQty()>0)
                return 0;
        }
        return 1;
    }

}